import os
from settings import ENCODING

class Document(object):
    def __init__(self, title, path, content=None):
        """
        :param title: name of file
        :param path: base path file
        """
        self._title = title
        self._path = path
        self._content = content

    @property
    def full_path(self):
        """
        :return: full path file
        """
        return os.path.join(self._path, self._title)

    def __str__(self):
        """
        :return: description of document
        """
        return "PATH:{} \nTITLE:{} \nCONTENT:{}".format(self._path, self._title, self._content)