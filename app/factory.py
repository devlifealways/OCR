from parser import Parser
from corpus import Corpus
from clustering import Clustering
from ocr import OCR
from physic_classification import Physic_classification
import settings


class Factory(object):
    def __new__(cls, *args, **kwargs):
        """
        :param args: 
        :param kwargs: 
        :return: 
        """
        return None

    @staticmethod
    def parser_instance(dir=settings.CORPUS_TXT_FILES):
        """
        :param dir: 
        :return: 
        """
        return Parser(dir)

    @staticmethod
    def corpus_instance(documents):
        """
        :param documents: 
        :return: 
        """
        return Corpus(documents)

    @staticmethod
    def clustering_instance(corpus, num_clusters=1):
        """
        :param corpus: 
        :param num_clusters: 
        :return: 
        """
        return Clustering(corpus, num_clusters)

    @staticmethod
    def ocr_instance(path, verbose=True, k=1):
        """
        :param path: 
        :param k: number of threads
        :return: 
        """
        return OCR(path, verbose, k)

    @staticmethod
    def physic_classification_instance(data, src, output_dir=settings.CORPUS_CLASSIFIED_PATH):
        """
        :param data: 
        :param src: 
        :param output_dir: 
        :return: 
        """
        return Physic_classification(data, src, output_dir)
