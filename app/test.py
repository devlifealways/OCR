import os
import unittest
from document import Document
from parser import Parser
from corpus import Corpus
from ocr import OCR
from clustering import Clustering
import settings

class DocumentTestCase(unittest.TestCase):

    def test_full_path(self):
        """
        :return:
        """
        d = Document('title','path')
        self.assertEqual(d.full_path, "{}/{}".format(d._path, d._title))

class ParserTestCase(unittest.TestCase):

    def setUp(self):
        """
        :return:
        """
        self.p = Parser(settings.CORPUS_PATH)
        self.p.load(load_content=False)


    def test_load(self):
        """
        :return:
        """
        self.assertNotEqual(len(self.p._documents), 0, "documents aren't loaded")

    def test_create_and_remove_dir(self):
        """
        :return:
        """
        dir='dir_test_to_remove'
        # create dir
        Parser.create_dir(dir)
        self.assertEqual(os.path.exists(dir), True, 'directory is not successfully created')
        # remove dir
        Parser.remove_dir(dir)
        self.assertEqual(os.path.exists(dir), False, 'directory is not successfully removed')

    def test_write_file(self):
        pass

    def test_read_excelfile(self):
        pass

    def test_get_extension(self):
        """
        :return:
        """
        file='testfile.m2sili.pdf'
        self.assertEqual(Parser.get_extension(file),'pdf')


class CorpusTestCase(unittest.TestCase):

    def setUp(self):
        ocr = OCR(settings.CORPUS_PATH, verbose=False, k=os.cpu_count())
        ocr.start_ocr_parallelization()
        # parsing data
        p = Parser(ocr._output_dir)
        p.load(load_content=True)
        # Create corpus
        self.corpus = Corpus(p._documents)


    def test_stop_words(self):
        """
        :return:
        """
        # self.assertNotEqual(len(self.corpus.stopwords), 0 , 'Stop words are empty')
        pass

    def test_total_vocab_stemmed(self):
        """
        :return:
        """
        self.assertNotEqual(len(self.corpus.total_vocab_stemmed), 0 , 'Stop words are empty')




class ClusteringTestCase(unittest.TestCase):
    def setUp(self):
        path = "Corpus-KaliConseil"
        ocr = OCR(path, verbose=False, k=os.cpu_count())
        ocr.start_ocr_parallelization()
        # parsing data
        p = Parser(ocr._output_dir)
        p.load(load_content=True)
        # Create corpus
        corpus = Corpus(p._documents)

        # kmeans clustering
        self.clustering = Clustering(corpus, num_clusters=5)
        self.clustering.KMeans()

    def test_kmeans(self):
        """
        :return:
        """
        self.assertNotEqual(self.clustering._kmeans, None,'kmeas in None')

    def test_num_clusters(self):
        """
        :return:
        """
        self.assertEqual(self.clustering._num_clusters, 5, 'num clusters not affected')

    def test_get_tfidf_vectorizer(self):
        """
        :return:
        """
        self.assertNotEqual(self.clustering._tfidf_vectorizer, None, 'tfidf_vectorizer is None')


    def test_get_tfidf_matrix(self):
        """
        :return:
        """
        self.assertNotEqual(self.clustering._tfidf_matrix, None, 'tfidf_matrix is None')


    def test_get_terms(self):
        """
        :return:
        """
        self.clustering.get_terms()
        self.assertNotEqual(self.clustering._terms, None, 'terms is None')


    def test_get_clusters(self):
        """
        :return:
        """
        self.assertNotEqual(len(self.clustering.get_clusters()), 0, 'get_clusters is empty')

    def test_get_words_by_cluster(self):
        """
        :return:
        """
        self.assertNotEqual(len(self.clustering.get_words_by_cluster(cluster_index=0)),0, 'words by cluster is empty')

    def test_get_data_clusters_as_dict(self):
        """
        :return:
        """
        self.assertNotEqual(len(self.clustering.get_data_clusters_as_dict()),0, 'data as dict is empty')



class OcrTestCase(unittest.TestCase):

    def test_pdf_to_image_bytes(self):
        pass

    def test_get_text_recognizes(self):
        pass



class Physic_classdicationTestCase(unittest.TestCase):

    def test_copy_files_worker(self):
        pass





class Indexer(unittest.TestCase):


    def test_add_documents(self):
        pass

    def test_index_my_docs(self):
        pass

    def test_clean_index(self):
        pass

    def test_incremental_index(self):
        pass

    def test_query_one_field(self):
        pass

    def test_query_multiple_fields(self):
        pass


if __name__ =="__main__":
     OCR(settings.CORPUS_PATH, verbose=True, k=os.cpu_count()).start_ocr_parallelization()
     unittest.main()

