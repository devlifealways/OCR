This is a clustring command application, using Kmeans as it main algorithm.
To use it, first please build docker's image :
# Docker instructions, please run it in the root directory
    $ docker build -t ocr:project .
# Once the image is built, log in with bash
    $ docker run -ti ocr:project bash
# Please refer to the help option to see how it works, but first go into the app folder 
	$ python main.py --help


