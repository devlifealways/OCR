from __future__ import print_function
import sys, json
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.externals import joblib
from corpus import Corpus
from parser import Parser
from extra_decorators import singleton
from settings import ENCODING


@singleton
class Clustering(object):
    def __init__(self, corpus, num_clusters=1):
        """
        :param corpus: 
        :param num_clusters: 
        """
        self._num_clusters = num_clusters
        self._corpus = corpus
        self._kmeans = None
        self._terms = None
        self._tfidf_vectorizer = None
        self._tfidf_matrix = None

    def get_tfidf_vectorizer(self, max_df=1.0, min_df=0.1, max_features=200000):
        """
        :param max_df: 
        :param min_df: 
        :param max_features: 
        :return: 
        """
        if not self._tfidf_vectorizer:
            self._tfidf_vectorizer = TfidfVectorizer(max_df=max_df, max_features=max_features,
                                                     min_df=min_df, tokenizer=self._corpus.tokenize_and_stem)
        return self._tfidf_vectorizer

    def get_tfid_matrix(self):
        """
        :return: 
        """
        if not self._tfidf_matrix:
            contents = [item._content for item in self._corpus._documents]
            self._tfidf_matrix = self.get_tfidf_vectorizer().fit_transform(contents)

        return self._tfidf_matrix

    def get_terms(self):
        """
        :return: 
        """
        if not self._terms:
            self._terms = self.get_tfidf_vectorizer().get_feature_names()
        return self._terms

    def KMeans(self):
        """
        :return: 
        """
        self._kmeans = KMeans(n_clusters=self._num_clusters)
        self._kmeans.fit(self.get_tfid_matrix())

    def get_clusters(self):
        """
        :param num_clusters: 
        :return: 
        """
        if not self._kmeans:
            self.KMeans()

        clusters = self._kmeans.labels_.tolist()
        return clusters

    def save(self, filename):
        """
        :param filename: 
        :return: 
        """
        if not self._kmeans:

            raise NameError('Clustering.save: Kmeans is attribut is None')
        else:
            joblib.dump(self._kmeans, filename)



    def get_order_centroids(self):
        """
        :return: 
        """
        # sort cluster centers by proximity to centroid
        if not self._kmeans:
            raise NameError('Clustering.get_order_centroids: Kmeans is attribut is None')
        else:
            order_centroids = self._kmeans.cluster_centers_.argsort()[:, ::-1]
            return order_centroids

    def get_words_by_cluster(self, cluster_index, number_words=1):
        """
        :param index: 
        :param number_words: 
        :return: 
        """
        words = []
        for centroid in self.get_order_centroids()[cluster_index, :number_words]:
            w = self.get_terms()[centroid]
            words.append(w)
        return words

    def get_docs_by_cluster(self, cluster_index):
        """
        :return: 
        """
        # get titles
        titles = [item._title for item in self._corpus._documents]
        # load data in dataframe
        docs = {'title': titles, 'cluster': self.get_clusters()}
        frame = pd.DataFrame(docs, index=[self.get_clusters()], columns=['title', 'cluster'])
        # filter by title
        frame_titles = frame.loc[cluster_index]['title']
        # return result
        return frame_titles if isinstance(frame_titles, str) else frame_titles.values

    def get_data_clusters_as_dict(self, number_words=1, save=False):
        """
        :return: 
        """
        data = {}
        if self._kmeans:

            for index_cluster in range(self._num_clusters):
                data[index_cluster] = (self.get_words_by_cluster(index_cluster, number_words=number_words),
                                       list(self.get_docs_by_cluster(index_cluster)))
            if save:
                with open('clustring.json', 'w') as fp:
                    json.dump(data, fp)
            return data
        else:
            print("Kmeans algorithm not yet called", file=sys.stderr)


if __name__ == "__main__":
    # Parsing data
    parser = Parser('corpus_txt_files')
    parser.load()

    # Create corpus
    corpus = Corpus(parser._documents)

    clustering = Clustering(corpus, num_clusters=2)
    clustering.KMeans()

    # show data
    # for index_cluster in range(clustering._num_clusters):
    #    print("Cluster : {}".format(clustering.get_words_by_cluster(index_cluster, number_words=1)))
    #    print("Docs : {}".format(clustering.get_docs_by_cluster(index_cluster)))
    #    print()


    data = clustering.get_data_clusters_as_dict()
    print(data)
