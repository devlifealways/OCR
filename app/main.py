import os
import sys
import getopt
from factory import Factory
import settings
from indexer import Indexer


def usage():
    print("This is a clustring command line application, which uses Kmeans as it's main algorithm")
    print("Usage: python3.5 main.py [options]")
    print("Options:")
    print("   -p  --path        Set source path documents location")
    print("   -t  --test        Run unit tests of application")
    print("   -v  --verbose     Start application with verbose mode")
    print("   -i  --indexing    Start indexing documents")
    print("   -s  --search      Set word for full search text")
    print("   -c  --clusters    Set number of clusters")
    print("   -w  --workers     Set number of threads")
    print("   -h  --help        show options")


def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "p:htvis:c:w:",
                                   ["path=", "help", "test", "verbose", "indexing", "search=", "clusters=", "workers="])
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit()

    verbose = False
    test = False
    indexing = False
    searching = False
    clusters = 5
    w = os.cpu_count()
    indexer = Indexer()

    for o, a in opts:
        if o in ("-p", "--path"):
            settings.CORPUS_PATH = a
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-t", "--test"):
            test = True
        elif o in ("-i", "--indexing"):
            indexing = True
        elif o in ("-s", "--search"):
            word = a
            searching = True
        elif o in ("-v", "--verbose"):
            verbose = True
        elif o in ("-c", "--clusters"):
            if int(a) > 0:
                clusters = int(a)
        elif o in ("-w", "--workers"):
            if int(a) > 0:
                w = int(a)
        else:
            assert False, "unhandled option"

    if (int(test) + int(searching) + int(indexing)) not in (0, 1):
        print("Flags are not correct ! ", file=sys.stderr)
        sys.exit()

    if test:
        os.system("python3.5 test.py")
    elif indexing:
        indexer.index_my_docs()
        print("indexing process was finished")
    elif searching:
        results = indexer.query_one_field("content", word)
        print('\n'.join([str(element) for element in results]) if verbose else results)


    else:
        ocr = Factory.ocr_instance(settings.CORPUS_PATH, k=w, verbose=verbose)
        ocr.start_ocr_parallelization()
        if verbose:
            print("OCR time: ", ocr._time, "s")

        # parsing data
        p = Factory.parser_instance(ocr._output_dir)
        p.load()

        # Create corpus
        corpus = Factory.corpus_instance(p._documents)

        # kmeans clustering
        clustering = Factory.clustering_instance(corpus, num_clusters=clusters)
        clustering.KMeans()

        # json data
        data = clustering.get_data_clusters_as_dict()

        pc = Factory.physic_classification_instance(data, src=settings.CORPUS_PATH)
        pc.start_parallel_physic_classification(verbose=verbose)
        if verbose:
            print("Physic classification time: ", pc._time, " s")
            print("Files are classified into directory: {}".format(pc._output_dir))


if __name__ == "__main__":
    main()
