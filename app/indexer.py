#!/usr/bin/env python
# -*- coding: utf-8 -*-

from whoosh.fields import *
from whoosh.qparser import QueryParser
from whoosh.analysis import StemmingAnalyzer
from whoosh.filedb.filestore import FileStorage
from whoosh.qparser import MultifieldParser

import settings

from factory import Factory
import os, sys


class Indexer(object):
    # the folder where the indexes will be saved
    INDEX_DIR = "index_dir"

    def __init__(self):
        """
           only the title and the path will be saved
           the text will be extracted directly from the file
        """
        # activate the steaming
        stem_ana = StemmingAnalyzer()
        self.schema = Schema(title=TEXT(analyzer=stem_ana, stored=True), path=ID(unique=True, stored=True),
                             content=TEXT(analyzer=stem_ana), time=STORED)
        self.storage = FileStorage(Indexer.INDEX_DIR)

        if not os.path.exists(Indexer.INDEX_DIR):
            os.mkdir(Indexer.INDEX_DIR)

        if not self.storage.index_exists():
            self.index_my_docs()
        else:
            self.ix = self.storage.open_index()

    def add_documents(self, writer):
        parser = Factory.parser_instance(settings.CORPUS_TXT_FILES)
        if not len(parser._documents):
            parser.load()

        for document in parser._documents:
            # to know which file should be modified
            modtime = os.path.getmtime(document._path)
            writer.add_document(title=document._title, path=document._path, content=document._content,
                                time=modtime)

    def index_my_docs(self, dirname=INDEX_DIR, clean=False):
        if clean or not self.storage.index_exists():
            self.clean_index(dirname)
        else:
            self.incremental_index(dirname)

    def clean_index(self, dirname):
        # Always create the index from scratch
        self.ix = self.storage.create_index(self.schema)
        writer = self.ix.writer()
        # Assume we have a function that gathers the filenames of the
        # documents to be indexed
        self.add_documents(writer)
        writer.commit(optimize=True)

    def incremental_index(self, dirname=INDEX_DIR):
        # load the index from the folder
        self.ix = self.storage.open_index()

        # The set of all paths in the index
        indexed_paths = set()
        # The set of all paths we need to re-index
        to_index = set()

        with self.ix.searcher() as searcher:
            writer = self.ix.writer()
            # Loop over the stored fields in the index
            for fields in searcher.all_stored_fields():
                indexed_path = fields['path']
                indexed_paths.add(indexed_path)

                if not os.path.exists(indexed_path):
                    # This file was deleted since it was indexed
                    writer.delete_by_term('path', indexed_path)

                else:
                    # Check if this file was changed since it
                    # was indexed
                    indexed_time = fields['time']
                    mtime = os.path.getmtime(indexed_path)
                    if mtime > indexed_time:
                        # The file has changed, delete it and add it to the list of
                        # files to reindex
                        writer.delete_by_term('path', indexed_path)
                        to_index.add(indexed_path)

            # Loop over the files in the filesystem
            # Assume we have a function that gathers the filenames of the
            # documents to be indexed
            self.add_documents(writer)
            # Have only one file
            writer.commit(optimize=True)

    def query_one_field(self, category, text):
        """
        Make a query to search the files
        :category: (title|path|content)
        :text: the content when searching
        :return: an array of results, best loop over it
        please to work with this function, encapsulate it with the famous with
        """
        ix = self.storage.open_index()
        # with self.ix.searcher() as searcher:
        #     query = QueryParser(category, self.ix.schema).parse(text)
        #     results = searcher.search(query)
        # return results
        # try:
        searcher = ix.searcher()
        query = QueryParser(category, self.ix.schema).parse(text)
        return searcher.search(query)


    def query_multiple_fields(self, text):
        ix = self.storage.open_index()
        # with ix.searcher() as searcher:
        #     mparser = MultifieldParser(["title", "content", "path"], schema=self.schema).parse(text)
        #     results = searcher.search(mparser)
        # return results
        try:
            searcher = ix.searcher()
            mparser = MultifieldParser(["title", "content", "path"], schema=self.schema).parse(text)
            return searcher.search(mparser)
        finally:
            searcher.close()


if __name__ == "__main__":
    print("Indexer class test cases : ")
    indexer = Indexer()




    # by default it will use INDEX_DIR
    # indexer.index_my_docs()

    #results = indexer.query_one_field("content", "facture")
    #print(*results)
    # print(results)
    #found = results.scored_length()
    #if results.has_exact_length():
    #    print("Scored", found, "of exactly", len(results), "documents")
    #else:
    #    low = results.estimated_min_length()
    #    high = results.estimated_length()

    #    print("Scored", found, "of between", low, "and", high, "documents")
