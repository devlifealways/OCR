import io
import os
import sys
import time
import pyocr
import pyocr.builders
from threading import Thread
from wand.image import Image
from PIL import Image as PI
from parser import Parser
from document import Document
from extra_decorators import singleton

import settings

class OCR_Thread(Thread):

    def __init__(self, documents, output_dir, verbose, lang='fra' ):
        """
        :param documents: 
        :param output_dir: 
        :param verbose: 
        :param lang: 
        """
        super(OCR_Thread, self).__init__()

        self._tool = pyocr.get_available_tools()[0]
        self._lang = lang
        # if language doesn't exit on system
        if self._lang not in self._tool.get_available_languages():
            url = "https://packages.debian.org/sid/all/tesseract-ocr-fra/download"
            message = "Language chosen doesn't exit please check this link for download: {} ".format(url)
            print(message, file=sys.stderr)
            sys.exit()

        self._documents = documents
        self._output_dir = output_dir
        self._verbose = verbose


    @staticmethod
    def pdf_to_image_bytes(path_file, resolution=300, format='jpeg'):
        """
        :param path_file: 
        :param resolution: 
        :param format: 
        :return: 
        """
        if not os.path.isfile(path_file):
            print("File doesn't exist",file=sys.stderr)
            return None

        image_pdf = Image(filename=path_file, resolution=resolution)
        img = image_pdf.convert(format)
        img_bytes = Image(image=img).make_blob(format)
        return img_bytes


    def get_text_recognizes(self, path_file):
        """
        :param path_file: 
        :return: 
        """
        img_bytes = OCR_Thread.pdf_to_image_bytes(path_file)
        txt = self._tool.image_to_string(PI.open(io.BytesIO(img_bytes)),lang=self._lang,builder=pyocr.builders.TextBuilder())
        return txt

    def run(self):
        """
        :return: 
        """
        for doc in self._documents:

                assert isinstance(doc, Document)

                if self._verbose:
                    print(doc.full_path, file=sys.stdout)
                text = ""
                extension_file = Parser.get_extension(doc._title)

                if extension_file in ('pdf'):
                    text = self.get_text_recognizes(doc.full_path)
                elif extension_file in settings.EXCEL_EXTENSIONS:
                    text = Parser.read_excelfile(doc.full_path)

                path = "{}/{}".format(self._output_dir, doc._title).replace(".{}".format(extension_file), ".{}".format(settings.CONVERT_EXTENSIONS_DICT[extension_file]))
                Parser.write_file(path, text)













@singleton
class OCR(object):

    def __init__(self, input_dir, verbose, k=1, output_dir=settings.CORPUS_TXT_FILES):
        """
        :param input_dir: 
        :param k: number of threads
        :param output_dir: 
        :param verbose: 
        """
        self._parser = Parser(input_dir)
        self._parser.load(load_content=False, extensions = ('pdf','xls','xlsx'))
        self._k = k
        self._time = 0
        self._output_dir = output_dir
        self._verbose = verbose


    def start_ocr_parallelization(self):
        """
        :return: 
        """

        if(Parser.create_dir(self._output_dir)):

            threads = []

            # number of documents
            count_docs = len(self._parser._documents)

            # Number of threads should be lower or equal to number of docs
            if count_docs < self._k:
                print('Number of threads cannot be greater than number of items',file=sys.stderr)
                sys.exit()

            # start time
            start = time.time()

            # size per thread
            size = int(count_docs/self._k)
            # thread rest calcul
            rest = count_docs%self._k
            # init vars min, max
            min, max = 0,0

            for i in range(1, self._k + 1):
                # get range min
                min += size * (0 if i == 1 else 1)
                # get range max
                max = i * size -1
                # get docs per thread
                docs = self._parser._documents[min:max+1]
                # append thread
                threads.append(OCR_Thread(docs,self._output_dir, self._verbose ))

            if rest > 0:
                # get range rest min
                rest_min = max+1
                # get range rest max
                rest_max = max+rest
                # get docs per thread
                docs = self._parser._documents[rest_min:rest_max+1]
                # append threads
                threads.append(OCR_Thread(docs, self._output_dir, self._verbose))

            # start threads
            for t in threads:
                t.start()
            # join threads
            for t in threads:
                t.join()

            # start time
            end = time.time()
            self._time = end-start
        elif self._verbose:
            print("Corpus is already converted, delete {} if you want to reconvert pdf files".format(self._output_dir))

