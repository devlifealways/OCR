__authors__ = ["hossam", "hamza"]
__emails__ = ["h.benhous@gmail.com", "rouineb.business@gmail.com"]

ENCODING = "utf-8"

EXCEL_EXTENSIONS = ('xls', 'xlsx')

CONVERT_EXTENSIONS_DICT = {'pdf': 'txt',
                           'xls': 'excel',
                           'xlsx': 'excelx'}

CORPUS_PATH = "Corpus-KaliConseil"

CORPUS_CLASSIFIED_PATH = "corpus_classified"

CORPUS_TXT_FILES = "corpus_txt_files"
